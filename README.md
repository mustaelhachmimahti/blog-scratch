# Blog From scratch

## Brève explication

Il s'agit d'un projet dans lequel nous devions créer un blog avec les informations qui ont été demandées à la base de données.

### Fonctionnement

1. Avant de lancer le projet, vous devrez ajouter vos propres données dans le fichier db-connection.php.

2. pour lancer le serveur, nous irons dans le projet cloné et dans le terminal nous mettrons la commande suivante `php -S 0.0.0.0.0:8000`

3. Nous avons la possibilité de filtrer les articles à la fois par catégorie et par auteur. Par exemple, si nous filtrons par `Harry Cover` et
   `Performance`, nous verrons les articles qui contiennent ces caractéristiques

4. Nous pouvons également afficher l'article complet en cliquant sur le bouton "Lire la suite".

### Style

La partie la plus esthétique laisse beaucoup à désirer mais comme ce n'était pas l'objectif principal, j'ai décidé de la laisser comme ça, tant qu'elle sépare les articles comme il se doit et qu'elle est responive, c'est suffisant pour moi.
