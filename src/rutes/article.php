<?php
include "./../db-connection.php"
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Blog from scratch</title>
    <link rel="stylesheet" href="./../styles/articleStyles.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Dongle">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
          <link rel="shortcut icon" type="image/png" href="./../images/favicon-32x32.png"/>
</head>
<body>
    <header>
        <div class="logo-title">
            <div class="logo"><img src="./../images/blogger-logo-icon-png-10164.png" alt="Blog Icon"></div>
            <div class="title"><h1>Blog From Scratch</h1></div>
        </div>
        <a style="margin-left: 100px" href="./../../index.php">Retour</a>
    </header>
<main> 
<?php 
$id_unic = $_GET['id'];
$sql_unic_article = "SELECT * FROM articles JOIN authors ON articles.author_id = authors.id WHERE articles.id = $id_unic";
$unic_respons = $conn->query($sql_unic_article);
?>
<?php
forEach($unic_respons as $unic_res){
  ?>
  <article>
      <div class="title"><h2><?php echo $unic_res["title"]?></h2></div>
      <div class="public">
        <p><?php echo $unic_res['publised_at']?></p><br><br><br>
    </div>
    <div class="image"> <img style="width: 50%; height: 50%;" src="<?php echo $unic_res['image_url']?>" alt="<?php echo $unic_res['image_alt']?>"></div>
    <div class="content"><?php echo $unic_res['content']?></div>
    <div class="author">
        <p>By <?php echo $unic_res['firstname'] . ' ' . $unic_res['lastname'] ?></p><br><br>
    </div>
    <?php
    $sql_rqs = "SELECT * FROM articles_categories JOIN categories ON categories.id = articles_categories.category_id WHERE articles_categories.article_id = $id_unic";
    $results5 = $conn->query($sql_rqs);
    while($result5 = $results5->fetch_assoc()){?>
    <div class="category"><p><?php echo $result5["category"]?></p></div>
    <?php }?>
    </div>
  </article>
<?php }?>
</main>
<footer><div><p>©2021 - blogfromscratch</p></div></footer>
</body>
</html>