<?php
require "./src/db-connection.php";
require "./src/querys/querys.php"?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Blog from scratch</title>
    <link rel="stylesheet" href="./src/styles/styles.css">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Dongle">
          <link rel="shortcut icon" type="image/png" href="./src/images/favicon-32x32.png"/>
</head>
<body>  

<header>
    <div class="logo-title">
        <div class="logo"><img src="./src/images/blogger-logo-icon-png-10164.png" alt="Blog Icon"></div>
        <div class="title"><h1>Blog From Scratch</h1></div>
    </div>
    <div class="filter">
        <div class="filter-by-author">
            <form action="index.php" method="POST">
            <select name="author">
            <option selected disabled hidden>Filter by autor</option>
            <?php forEach($results2 as $result2){?>
                <option value="<?php echo $result2['firstname'] ?>"><?php echo $result2['firstname'] ?> <?php echo $result2['lastname'] ?></option><?php }?>  
            </select>
        </div>
        <div class="filter-by-category">
        <select name="category">
            <option selected disabled hidden>Filter by category</option>
            <?php forEach($results4 as $result4){?>
                <option value="<?php echo $result4['category'] ?>"><?php echo $result4['category'] ?></option><?php }?>     
            </select>
            <input type="submit" value="filter" name="filter_1">
        </div>
    </div>
    </header><br><br><br>
    <?php 

    if(isset($_POST['filter_1']) && $_POST['author'] != NULL){  
        $filter_author_term =  $_POST['author'];
        $sql_authors_articles .= "WHERE authors.firstname = '{$filter_author_term}' ORDER BY publised_at ASC" ;
        $results1 = $conn->query($sql_authors_articles); 
    }
    if (isset($_POST['filter_1'])&& $_POST['category'] != null) {
        $filter_category_term = $_POST['category'];
        $sql_authors_articles = "SELECT * FROM articles JOIN authors ON articles.author_id = authors.id JOIN articles_categories ON articles_categories.article_id = articles.id JOIN categories ON articles_categories.category_id = categories.id WHERE categories.category = '{$filter_category_term}' ORDER BY publised_at ASC";
        $results1 = $conn->query($sql_authors_articles);
    }
    ?>
<main>
<?php forEach($results1 as $result1){?>
    <?php $id = $result1["art_id"]
    ?>
    <article>
        <div class="article-stuff">
        <h3><?php echo $result1["title"]?></h3>

    <div class="author-public">
        <p>By <?php echo $result1['firstname'] . ' ' . $result1['lastname'] ?></p>
        <p><?php echo $result1['publised_at']?></p>
    </div>
    <div class="content"><?php echo $result1['content']?></div> 
    <a style="display: flex; justify-content: right;" href="./src/rutes/article.php?id=<?php echo $id?>">Lire la suite</a><br>
    <?php 
        $sql_rqs = "SELECT * FROM articles_categories JOIN categories ON categories.id = articles_categories.category_id WHERE articles_categories.article_id = $id";
        $results5 = $conn->query($sql_rqs);
        while($result5 = $results5->fetch_assoc()){?>
        <div class="category" ><a href=""><?php echo $result5["category"]?></a></div>
        <input type="hidden" value="<?php echo $result5["category"]?>" name="comp_cat<?php echo $id?>">
        <?php }?>
        </div>
    </article>
    <?php }?>
    </form>     
</main>
<footer><div><p>©2021 - blogfromscratch</p></div></footer>
</body>
</html>